package main;

public class App {
	public static void main(String[] args) {
		Motor motor = new Motor();
		Visor visor = new Visor();
		Calculador calculador = new Calculador();
		
		int[] sorteios = new int[3];
		
		for(int i = 0; i < 3; i++) {
			sorteios[i] = motor.sortear();
		}
		
		int pontos = calculador.calcular(sorteios);
		
		visor.imprimir(sorteios, pontos);
	}
}
