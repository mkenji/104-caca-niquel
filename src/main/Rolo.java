package main;

public class Rolo {
	private static String[] valores = {"Ameixa", "Pêssego", "Maçã", "Pera", "Uva", "Jaca", "Atemóia", "7"};
	
	public static int getTamanho() {
		return valores.length;
	}
	
	public static String getValor(int indice) {
		return valores[indice];
	}
}
