package main;

public class Calculador {
	
	public int calcular(int[] sorteios) {
		int[] contagem = new int[Rolo.getTamanho()];
		
		for(int i = 0; i < contagem.length; i ++) {
			contagem[i] = 0;
		}
		
		for(int numero: sorteios) {
			contagem[numero]++;
		}
		
		for(int casa: contagem) {
			if(casa == 3) {
				return 1000;
			}
			
			if(casa == 2) {
				return 100;
			}
		}
		
		return 0;
	}
}
